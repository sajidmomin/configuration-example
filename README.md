# Configuration Examples #
This configuration examples demos two ways to manage property files.  One apporach is to use the OSGi container and the other way is working with Spring beans.  The below instructions are required to setting up this example.

## Setting up Digital Factory environment variables ##
We need to set an environment variable `df.env` so that configuration example services in Digital Factory load the correct properties file.  Example, `ExampleService.java`.  The steps are below:
1. Log into local instance
2. To set up environment properties globally:
    a. `vi /etc/profile.d/java.sh`.  You might need to do `sudo` if you don't have access to create the file.
        i. add line `export CATALINA_OPTS="-Ddf.env=<env-id>"`
3. To set up environment properties for the logged in user:
    a. `vi ~/.bash_profile`
        i. add line `export CATALINA_OPTS="-Ddf.env=<env-id>"`
4. If using a daemon service, make sure to `export CATALINA_OPTS="-Ddf.env=<env-id>"` in the script.
Environment IDs
    * dev (default), this is used by local development environments and INT environment.
    * qa
    
## How do I get set up? ##

* This module requires SCR: http://felix.apache.org/documentation/subprojects/apache-felix-service-component-runtime.html
* Add additional plugins to the pom file:
```
<plugin>
    <groupId>org.apache.felix</groupId>
    <artifactId>maven-scr-plugin</artifactId>
    <version>1.15.0</version>
    <executions>
        <execution>
        <id>generate-scr-scrdescriptor</id>
        <goals>
            <goal>scr</goal>
        </goals>
        </execution>
    </executions>
</plugin>
```
* Add additional dependencies to the pom file:
```
<dependency>
    <groupId>org.apache.felix</groupId>
    <artifactId>org.apache.felix.scr.annotations</artifactId>
    <version>1.9.12</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>org.apache.felix</groupId>
    <artifactId>org.apache.felix.scr.ds-annotations</artifactId>
    <version>1.2.8</version>
    <scope>provided</scope>
</dependency>
```    
* Install Apache Felix Declarative Services (org.apache.felix.scr) bundle to the Felix OSGi container.