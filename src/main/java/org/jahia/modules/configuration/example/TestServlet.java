package org.jahia.modules.configuration.example;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.jahia.modules.configuration.example.service.ExampleService;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by smomin on 1/21/16.
 */
@Component(
        label = "Test Servlet to test out configuration services",
        immediate = true
)
public class TestServlet  extends HttpServlet {

    private static final String PATH_TEST_CONFIGURATION = "/test/configuration";
    private static final Logger LOGGER = LoggerFactory.getLogger(TestServlet.class);

    @Reference
    private HttpService httpService;

    @Reference
    private ExampleService exampleService;

    @Activate
    protected void start(final ComponentContext ctx) {
        try {
            httpService.registerServlet(PATH_TEST_CONFIGURATION, this, null, null);
            LOGGER.info("Successfully registered TestServlet");
        } catch (ServletException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (NamespaceException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Deactivate
    protected void stop(final BundleContext bundleContext) {
        httpService.unregister(PATH_TEST_CONFIGURATION);
        LOGGER.info("Successfully unregistered TestServlet");
    }

    @Override
    protected void doGet(final HttpServletRequest request,
                         final HttpServletResponse response) throws ServletException, IOException {

        response.getWriter().write(exampleService.getApiUrl());
        response.getWriter().write("\n");
        response.getWriter().write(exampleService.getConfigurationProperties());

    }
}
