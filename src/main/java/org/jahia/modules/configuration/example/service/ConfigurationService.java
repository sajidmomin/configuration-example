package org.jahia.modules.configuration.example.service;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by smomin on 1/14/16.
 */
@Component(
        label = "Configuration Service",
        description = "This is a configuration service to demo configuration",
        immediate = true,
        metatype = true
)
@Service(value = ConfigurationService.class)
public class ConfigurationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationService.class);

    @Property(
            label = "API Username",
            description = "This is the api username.",
            value="root"

    )
    private static final String API_USERNAME_PARAM_NAME = "api.username";
    private String apiUsername;

    @Property(
            label = "API Password",
            description = "This is the api password.",
            value="root"
    )
    private static final String API_PASSWORD_PARAM_NAME = "api.password";
    private String apiPassword;

    public String getApiUsername() {
        return apiUsername;
    }

    public String getApiPassword() {
        return apiPassword;
    }

    /**
     *
     * @param ctx
     */
    @Activate
    protected void start(final ComponentContext ctx) {
        LOGGER.info("Successfully registered ConfigurationService");
        final Dictionary properties = ctx.getProperties();
        final Enumeration keys = properties.keys();
        final Map<String, Object> map = new HashMap<String, Object>();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            map.put(key, properties.get(key));
        }
        update(map);
    }

    /**
     *
     * @param bundleContext
     */
    @Deactivate
    protected void stop(final BundleContext bundleContext) {
        LOGGER.info("Successfully unregistered ConfigurationService");
    }

    /**
     * This method will be executed when the .cfg is updated.
     *
     * @param properties
     */
    @Modified
    protected void update(final Map<String, Object> properties) {
        this.apiUsername = (String) properties.get(API_USERNAME_PARAM_NAME);
        LOGGER.info("apiUsername is {}", apiUsername);

        this.apiPassword = (String) properties.get(API_PASSWORD_PARAM_NAME);
        LOGGER.info("apiPassword is {}", apiPassword);
    }
}
