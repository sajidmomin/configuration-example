package org.jahia.modules.configuration.example.service;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by smomin on 1/13/16.
 */
@Component(
        label = "Example Service",
        description = "This is an example service to demo configuration",
        immediate = true,
        metatype = true
)
@Service(value = ExampleService.class)
public class ExampleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleService.class);

    @Property(
            label = "API Url",
            description = "This is the api url key.",
            value="http://localhost:8080"
    )
    private static final String API_URL_PARAM_NAME = "api.url";
    private String apiUrl;

    @Reference
    private ConfigurationService configurationService;

    public String getConfigurationProperties() {
        return configurationService.getApiUsername() + ", " + configurationService.getApiPassword();
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(final String apiUrl) {
        this.apiUrl = apiUrl;
    }

    /**
     *
     * @param ctx
     */
    @Activate
    protected void start(final ComponentContext ctx) {
        LOGGER.info("Successfully registered ExampleService");
        final Dictionary properties = ctx.getProperties();
        final Enumeration keys = properties.keys();
        final Map<String, Object> map = new HashMap<String, Object>();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            map.put(key, properties.get(key));
        }
        update(map);
    }

    /**
     *
     * @param bundleContext
     */
    @Deactivate
    protected void stop(final BundleContext bundleContext) {
        LOGGER.info("Successfully unregistered ExampleService");
    }

    /**
     * This method will be executed when the .cfg is updated.
     *
     * @param properties
     */
    @Modified
    protected void update(final Map<String, Object> properties) {
        this.apiUrl = (String) properties.get(API_URL_PARAM_NAME);
        LOGGER.info("apiUrl is {}", apiUrl);
    }
}
