package org.jahia.modules.configuration.example;

import org.apache.commons.collections.CollectionUtils;
import org.jahia.osgi.BundleResource;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

/**
 * This class is executed the bundles is activated or deactivated.
 */
public class Activator implements BundleActivator {

    private static final String CFG_EXTENSTION = ".cfg";
    private static final String PROPERTY_DF_ENV = "df.env";

    private static final Logger LOGGER = LoggerFactory.getLogger(Activator.class);
    private final List<Configuration> configurationList = new ArrayList<>();

    /**
     * This method is executed when the bundle is started.
     *
     * @param bundleContext
     * @throws Exception
     */
    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        // Get environment variable value otherwise default to dev
        final String env = (System.getProperty(PROPERTY_DF_ENV) != null) ?
                System.getProperty(PROPERTY_DF_ENV) : "dev";
        LOGGER.debug("env type is {}", env);

        // Create the folder path using the environment variable to get all the properties files.
        final String configFolder = "META-INF/config/" + env;
        LOGGER.debug("configFolder is {}", configFolder);

        final Bundle bundle = bundleContext.getBundle();
        // Get all cfg files from the environment config folder.
        final Enumeration<URL> urls = bundle.findEntries(configFolder, "*" + CFG_EXTENSTION, false);

        // loads the configuration properties of services.
        loadConfiguration(bundle, urls);
    }

    /**
     * This method is executed when the bundle is stopped.
     *
     * @param bundleContext
     * @throws Exception
     */
    @Override
    public void stop(final BundleContext bundleContext) throws Exception {
        if (CollectionUtils.isNotEmpty(configurationList)) {
            // Loop through the file path and if exists, remove the file from the
            // /EnterpriseDistribution-DigitalFactory7100-SDK/digital-factory-data/modules location.
            for (final Configuration configuration : configurationList) {
                configuration.delete();
            }
        }
    }

    /**
     * This method loops through the files found and uses the name of the file to get the configuration to load
     * the properties in the .CFG file.
     *
     * @param bundle
     * @param urls
     * @throws IOException
     */
    private void loadConfiguration(final Bundle bundle,
                                   final Enumeration<URL> urls) throws IOException {
        if (urls != null) {
            final BundleContext bundleContext = bundle.getBundleContext();
            final ServiceReference configurationAdminReference =
                    bundleContext.getServiceReference(ConfigurationAdmin.class.getName());

            if (configurationAdminReference != null) {
                final ConfigurationAdmin configurationAdmin = (ConfigurationAdmin) bundleContext
                        .getService(configurationAdminReference);

                // Loop through the urls.
                while (urls.hasMoreElements()) {
                    final URL url = urls.nextElement();
                    LOGGER.debug("url is {}", url);

                    // Get the BundleResource from the url.
                    final BundleResource resource = new BundleResource(url, bundle);
                    final String fileName = resource.getFilename();
                    LOGGER.debug("fileName is {}", fileName);

                    // Remove the CFG from the file name to use to get the configuration object for the service.
                    final Configuration configuration = configurationAdmin
                            .getConfiguration(fileName.replace(CFG_EXTENSTION, ""));
                    if (configuration != null) {
                        final Properties properties = new Properties();
                        properties.load(resource.getInputStream());
                        final Dictionary dictionary = new Hashtable<Object, Object>(properties);
                        configuration.update(dictionary);
                    }
                    configurationList.add(configuration);
                }
            }
        }
    }
}
